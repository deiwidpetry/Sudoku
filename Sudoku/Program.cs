﻿using System;
using System.Linq;
using System.IO;

namespace Sudoku
{

    class Program
    {
        static void Main(string[] args)
        {
            SudokuForcaBruta oSudokuForcaBruta = new SudokuForcaBruta();
            string saida; 
            string entrada;//"200000035050019640000040700000002060040081590009750000900800000000020080005000003"
            int[] resultado;
            bool tamanhoValido = false;

            while (!tamanhoValido)
            {
                Console.WriteLine("Digite uma entrada com 81 digitos:");
                entrada = Console.ReadLine();

                tamanhoValido = entrada.Length.Equals(81);

                if (tamanhoValido)
                {
                    resultado = oSudokuForcaBruta.ForcaBruta(entrada);

                    saida = resultado.Select((valor, indice) => valor.ToString() + ((indice + 1) % 9 == 0 ? Environment.NewLine : "")).Aggregate((total, atual) => total + atual);


                    StreamWriter sw = new StreamWriter("ResultadosSudoku.txt");
                    sw.WriteLine();
                    sw.WriteLine($"Entrada: {entrada}");
                    sw.WriteLine("Resultado");
                    sw.WriteLine(saida);
                    sw.Close();
                }
                else
                {
                    Console.WriteLine("Entrada Invalida");
                }
            }
            
        }
    }
}
