﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    public class SudokuForcaBruta
    {
        protected class Celula
        {
            public int coluna { get; private set; }
            public int linha { get; private set; }
            public int bloco { get; private set; }
            public bool resolvido { get; private set; }
            public int valor { get; set; }

            public Celula(int posicao, int pValue)
            {
                linha = posicao / 9;
                coluna = posicao - linha * 9;
                bloco = (linha / 3) * 3 + coluna / 3;
                valor = pValue;
                resolvido = pValue != 0;
            }

            public override string ToString()
            {
                return string.Format("{0},{1},{2}", coluna, linha, bloco);
            }
        }

        //Armazena as celulas
        private List<Celula> grid;

        public int[] ForcaBruta(string pData)
        {
            //Popular celulas
            grid = pData.ToCharArray().Select((itm, ind) => new Celula(ind, Convert.ToInt32(itm) - 48)).ToList();

            //lista vinculada de células não resolvidas
            LinkedList<Celula> CelulasNaoResolvidas =
                new LinkedList<Celula>
                    (
                        grid.Where(c => !c.resolvido)
                        .OrderBy(c => c.linha)
                        .ThenBy(c => c.coluna)
                        .AsEnumerable()
                    );


            //Celula atual que está sendo examinada
            LinkedListNode<Celula> CelulaAtual = CelulasNaoResolvidas.Find(grid.First(c => !c.resolvido));

            //inicia o solucionador
            do
            {
                CelulaAtual.Value.valor =
                    CelulaNaoResolvidaPegarProximoValor(CelulaAtual.Value);

                //0 indicates no possible value exists for the current cell
                while (CelulaAtual.Value.valor == 0)
                {
                    //pega a célula anterior
                    CelulaAtual = CelulaAtual.Previous;

                    //pega seu valor
                    CelulaAtual.Value.valor =
                            CelulaNaoResolvidaPegarProximoValor(CelulaAtual.Value);
                }

                CelulaAtual = CelulaAtual.Next;

            } while (CelulaAtual != null);// quando nulo, indica que o fim da lista foi atingido

            return ObterGrid();

        }

        // Obtem os valores das células da Grid genérica como uma matriz de inteiros
        public int[] ObterGrid()
        {
            //retornar a grade resolvida
            return grid.OrderBy(c => c.linha)
                        .ThenBy(c => c.coluna)
                        .Select(c => c.valor)
                        .ToArray();
        }

        private int CelulaNaoResolvidaPegarProximoValor(Celula celula)
        {
            return Enumerable.Range(1, 9).Except
                (
                    //Pega todos os valores dos vizinhos da célula fornecida que tenham um valor maior que 0
                    grid.Where(cl => cl.valor > 0 & (cl.coluna == celula.coluna | cl.linha == celula.linha | cl.bloco == celula.bloco))
                        .Where(cl => cl != celula)
                        .Select(cl => cl.valor)

                ).FirstOrDefault(v => v > celula.valor);
        }
    }
}
